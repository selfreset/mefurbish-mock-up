var selfReset = {

  target: $('.dashboard'),

  initialize: function() {
    //switches to splash view and binds the navigation event listeners.
    var app = this;

    app.changeScript('splash_page');

    $('#loginNav').off('click').on('click', function() {
      app.changeScript('login');
    });
    $('#settingsNav').off('click').on('click', function() {
      app.changeScript('settings');
    });
    $('#motivationNav').off('click').on('click', function() {
      app.changeScript('motivation');
    });
    $('#remindersNav').off('click').on('click', function() {
      app.changeScript('reminders');
    });
    $('#moneyNav').off('click').on('click', function() {
      app.changeScript('money');
    });
    $('#progressNav').off('click').on('click', function() {
      app.changeScript('progress');
    });
    $('#calendarNav').off('click').on('click', function() {
      app.changeScript('calendar');
    });
    $('#socialList').hover(function() {
      $('#socialList a').css({ opacity: 100 });
      $('.main-sidebar').css('padding-left', '200px');
      $(this).addClass('stretchLeft');
    }, function() {
      $('#socialList a').css({ opacity: 0 });
      $('.main-sidebar').css('padding-left', '316px');
      $(this).removeClass('stretchLeft');
    });
  },

  bindListeners: {
    splash_page: function() {
      var app = selfReset;
    },
    login: function() {
      var app = selfReset;
      //binds all event listeners; sets initial script
      // $('.dashboard').attr('id', 'login');
      $('#loginButton').off('click').on('click', function() {
        app.changeScript('settings');
      });
    },
    settings: function() {
      var app = selfReset;
      // $('.dashboard').attr('id', 'settings');
      $('#settingsButton').off('click').on('click', function() {
        app.changeScript('motivation');
      });
    },
    motivation: function() {
      var app = selfReset;
      // $('.dashboard').attr('id', 'motivation');
      $('#createMessageButton').off('click').on('click', function() {
        var message = $('#message').val();
        $('#inspMessage').append('<li>' + message + '</li>');
        $('#message').val('');
      });
    },
    reminders: function() {
      var app = selfReset;
      $('#createReminderButton').off('click').on('click', function() {
        var reminder = $('#personal').val();
        $('#newReminder').show();
        $('#reminderMessage').append(reminder);
      });
    },
    money: function() {

    },
    progress: function() {

    },
    calendar: function() {

    }
  },

  changeScript: function(scriptId) {
  //switches between scripts
    this.target.empty().html($('#' + scriptId).html());
    console.log(scriptId);
    this.bindListeners[scriptId]();

  },



}

selfReset.initialize();
