# README #

This is a "mock-up" of a mobile app that helps you break bad habits. It demonstrates some of the ways the app would be used. 


# SETUP # 

To view, clone the repo and open index.html page in your browser. 


# INFO #

This site is a single-page app written in vanilla JavaScript with jQuery and CSS.